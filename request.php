<?php
//Load composer's autoloader
require 'vendor/autoload.php';

$requestParams = [
    'Имя' => $_REQUEST['name'],
    'E-mail' => $_REQUEST['email'],
    'Телефон' => $_REQUEST['phone'],
    'Дата мероприятия' => $_REQUEST['event-date'],
    'Сообщение' => $_REQUEST['message'],
];

$requestString = '<ul>';
foreach ($requestParams as $key => $requestParam) {
    $requestString .= '<li>' . $key . ': ' . $requestParam . '</li>';
}
$requestString .= '</ul>';

// Create the Transport
$transport = (new Swift_SmtpTransport('mail.nic.ru', 25))
    ->setUsername('postmaster@wedding-moscow.com')
    ->setPassword('N@45o|jQ')
;

// Create the Mailer using your created Transport
$mailer = new Swift_Mailer($transport);

// Create a message
$message = (new Swift_Message('Запрос с сайта'))
    ->setFrom(['postmaster@wedding-moscow.com'])
    ->setTo(['sashamks@mail.ru'])
    ->setBody($requestString, 'text/html')
    ->addPart(implode("\n", $requestParams), 'text/plain')
;

// Send the message
$failures = [];
if (!$mailer->send($message, $failures)) {
    echo "Что-то пошло не так. Попробуйте еще раз.";
} else {
    echo "Спасибо, Ваш запрос принят.";
}
