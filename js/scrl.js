jQuery(document).ready(function($) {
	$(function() {
		$('nav a').on('click', function(e) {
			if ($(window).width() < 821) {
				$('.main-menu').toggleClass('toggled');
				$('#menu-toggle').toggleClass('open');
			}
			e.preventDefault();
			$('html, body').animate({
				scrollTop: $($(this).attr('href')).offset().top - 80 + 'px'
			}, 1000, 'swing');
		});

		$('a.request').on('click', function (e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 80 + 'px'
            }, 1000, 'swing');
        })
	});
});