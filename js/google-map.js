(function($) {
	function new_map($el) {
		var $markers = $el.find('.marker');
		var args = {
			center: new google.maps.LatLng(0, 0),
			streetViewControl: false,
			mapTypeControl: false,
			scaleControl: false,
			zoomControl: true,
			scrollwheel: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		};
		var map = new google.maps.Map($el[0], args);
		map.markers = [];
		$markers.each(function() {
			add_marker($(this), map);
		});
		center_map(map);
		return map;
	}

	function add_marker($marker, map) {
		var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
		var icon = {
			path: "M-281 324c-26.9 0-48.7 21.8-48.7 48.7 0 8.6 2.3 16.7 6.3 23.8 0.7 1.2 1.3 2.3 2.1 3.4l40.3 70.1 40.3-70.1c0.6-0.9 1.2-1.9 1.7-2.9l0.4-0.6c3.9-7 6.3-15.1 6.3-23.8C-232.3 345.8-254.1 324-281 324zM-281 348.3c13.4 0 24.3 10.9 24.3 24.3 0 13.4-10.9 24.3-24.3 24.3 -13.4 0-24.3-10.9-24.3-24.3C-305.3 359.2-294.4 348.3-281 348.3z",
			fillColor: '#111',
			fillOpacity: 1,
			anchor: new google.maps.Point(-250, 450),
			strokeWeight: 0,
			scale: 0.25
		}
		var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			icon: icon
		});
		map.markers.push(marker);
		if ($marker.html()) {
			var infowindow = new google.maps.InfoWindow({
				content: $marker.html()
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, marker);
			});
		}
	}

	function center_map(map) {
		var bounds = new google.maps.LatLngBounds();
		$.each(map.markers, function(i, marker) {
			var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
			bounds.extend(latlng);
		});
		if (map.markers.length == 1) {
			map.setCenter(bounds.getCenter());
			map.setZoom(14);
		} else {
			map.fitBounds(bounds);
		}
	}
	var map = null;
	$(document).ready(function() {
		$('.google-map').each(function() {
			map = new_map($(this));
		});
	});
})(jQuery);