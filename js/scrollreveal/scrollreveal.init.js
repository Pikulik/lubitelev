window.sr = ScrollReveal({
	duration: 1000,
	origin: 'bottom',
	distance: '50px',
	scale: 1,
	reset: false
});
sr.reveal('.editor, .ani, .works-slider, .google-map, .entry, .pan, .contacts, .instagram, .columns-info, .columns, .video-gallery-info, .photoswipe-wrapper');
sr.reveal('.footer', {
	distance: '0px'
});