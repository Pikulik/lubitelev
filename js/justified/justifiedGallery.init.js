jQuery(document).ready(function($) {
	doTheGrid();
	$(window).resize(function() {
		doTheGrid();
	});
	function doTheGrid() {
		var w = $(window).width();
		if (w > 700) {
			rowH = 200;
		} else {
			rowH = 100;
		}
		$(".gallery.gallery-size-medium").justifiedGallery({
			selector: 'figure',
			rowHeight: rowH,
			justifyThreshold: .1,
			margins: 0,
			maxRowHeight: 300
		});
	}
});