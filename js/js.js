jQuery(document).ready(function($) {
	// popups
	$('.popup-button').click(function() {
		var a = 'div.' + $(this).attr('id');
		$(a).fadeIn("normal");
	});
	$(document).keyup(function(e) {
		if (e.keyCode === 27) {
			$('.popup').fadeOut("normal");
		};
	});
	$(".popup-content .close").click(function() {
		$('.popup').fadeOut("normal");
	});
	//parallax
	$(window).load(function() {
		$(".parallax-image").each(function() {
			var parallax = document.querySelector(".parallax-image");
			window.addEventListener("scroll", function() {
				var scrolledHeight = window.pageYOffset;
				var y = -(scrolledHeight) / 8 + "px";
				parallax.style.backgroundPosition = 'center ' + y;
			});
		});
	});
	//scrollup button
	$(window).scroll(function() {
		if ($(this).scrollTop() > 30) {
			$('.scrollup').fadeIn();
			$('header').addClass('scroll');
		} else {
			$('.scrollup').fadeOut();
			$('header').removeClass('scroll');
		}
	});
	$('.scrollup').click(function() {
		$("html, body").animate({
			scrollTop: 0
		}, 600);
		return false;
	});
	// works slider
	$('.works-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [{
			breakpoint: 700,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 414,
			settings: {
				slidesToShow: 1,
			}
		}, ]
	});
	$('.marker').show();
	// nav click scroll
	$(function() {
		if (window.location.hash) {
			$('html, body').animate({
				scrollTop: $(window.location.hash).offset().top - 80 + 'px'
			}, 1000, 'swing');
		}
	});
	//fotorama parallax
	$(window).load(function() {
		if (document.getElementById("slider")) {
			$(".fotorama img").each(function() {
				var parallax = document.querySelector(".fotorama img");
				window.addEventListener("scroll", function() {
					var scrolledHeight = window.pageYOffset;
					var y = (scrolledHeight) / 4 + "px";
					parallax.style.marginTop = y;
				});
			});
			$('.fotorama').on('fotorama:showend', function(e, fotorama) {
				$(".fotorama__active img").each(function() {
					var parallax = document.querySelector(".fotorama__active img");
					window.addEventListener("scroll", function() {
						var scrolledHeight = window.pageYOffset;
						var y = (scrolledHeight) / 4 + "px";
						parallax.style.marginTop = y;
					});
				});
			});
		}
	});
});