jQuery(document).ready(function($) {
	var on = 0;
	function loadPopup() {
		if (on == 0) {
			$("#post-popup").fadeIn(500);
			on = 1;
		}
	}
	function off() {
		if (on == 1) {
			$("#post-popup").fadeOut("normal");
			on = 0;
		}
	}
	$("#post-popup .close").click(function() {
		off();
		$('#post-popup-content').html("");
	});
	$(document).keyup(function(e) {
		if (e.keyCode === 27) {
			off();
			$('#post-popup-content').html("");
		};
	});
	$(".ajax").click(function() {
		loadPopup();
		var post_id = $(this).attr('data-id');
		var ajaxURL = MyAjax.ajaxurl;
		$.ajax({
			type: 'POST',
			url: ajaxURL,
			data: {
				'post_id': post_id,
				'action': 'ay_ajax'
			},
			success: function(result) {
				$('#post-popup-content').append(result);
			},
			error: function() {
				alert("Что-то пошло не так");
			}
		});
	});
});