$(document).ready(function () {

    $('.request-button').click(function() {
        $(':input[type="submit"]').prop('disabled', false);
        $('#myResultsDiv').html('');
    });

    $('#form').on('submit', function(e) {
        e.preventDefault(); // prevent native submit
        $(':input[type="submit"]').prop('disabled', true);
        $(this).ajaxSubmit({
            target: '#myResultsDiv'
        });
        document.getElementById("form").reset();

    });
    // Configure/customize these variables.
    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Читать полностью <i class='fa fa-angle-down'></i>";
    var lesstext = "Свернуть <i class='fa fa-angle-up'></i>";


    $('.more').each(function () {
        var content = $(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

});