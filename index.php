<?php
require __DIR__ . '/vendor/autoload.php';
$client_id = '511a5c45c20e744a5d4c287cdfc7e724a0bfadda';
$client_secret = 'RPKy27PP6IwD3cSEWR+4fXEe0iGRprgWSyzVygmFwq1qGgHIBWs5aKfzgT6s1XzY4Tgh38TkrdxqiP8WWaLOsKFgz17/Hm2o8FIOgGKs21Kgf04X0++1B1H28+lzK6vK';
$lib = new \Vimeo\Vimeo($client_id, $client_secret);
$albumId = 4980055;
$albumVideos = $lib->request('/me/albums/' . $albumId . '/videos', ["sort" => "manual"], 'GET');

$companiesLogos = [
    '/images/companies/01.jpg',
    '/images/companies/02.jpg',
    '/images/companies/03.jpg',
    '/images/companies/04.jpg',
    '/images/companies/05.jpg',
    '/images/companies/06.jpg',
    '/images/companies/07.jpg',
    '/images/companies/08.jpg',
    '/images/companies/09.jpg',
    '/images/companies/10.jpg',
];

$feedbacks = [
    [
        'link' => 'https://vk.com/kurateva2010',
        'name' => 'Юлия Наместникова',
        'message' => 'Спасибо вам огромное очень понравилось!!!',
        'photo' => '/images/feedbacks/1.jpg',
    ],
    [
        'link' => 'https://vk.com/olgavyselko',
        'name' => 'Ольга Ульяненкова',
        'message' => 'Александр огромное спасибо за вашу превосходную работу, все безумно понравилось!!!!!!!! каждый раз пересматривая видео испытываем бурю позитивных эмоций, невозможно нарадоваться вашей работой))))Спасибо за подаренные эмоции и замечательную работу!!!)))))',
        'photo' => '/images/feedbacks/2.jpg',
    ],
    [
        'link' => 'https://vk.com/leka8294',
        'name' => 'Валерия Киреева',
        'message' => 'Спасибо большое:)смотрели и хлюпали носами:)все очень трогательно и мило:)получили удовольствие,как в день свадьбы:)',
        'photo' => '/images/feedbacks/3.jpg',
    ],
    [
        'link' => 'https://vk.com/id97154589',
        'name' => 'Нармин Малышко',
        'message' => 'Сашенька,спасибо большое за такую замечательную работу, мы очень довольны.Было столько эмоций,классно)))Молодец))',
        'photo' => '/images/feedbacks/4.jpg',
    ],
    [
        'link' => 'https://vk.com/id59322127',
        'name' => 'Светлана Хвостункова',
        'message' => 'Саша,спасибо огромное за такую классную работу!!!!! Нам о-о-очень понравилось!Видео безумно красивое!!! Успехов в работе!)))',
        'photo' => '/images/feedbacks/5.jpg',
    ],
    [
        'link' => 'https://vk.com/id2005594',
        'name' => 'Юлия Малышева',
        'message' => 'Саша!!! Спасибо Вам огромное за отличную работу!!!!! Смотрели и нарадоваться не могли!!! Ты классный профессионал,знающий свою работу!!!! Нам очень понравились видео!!!! Спасибо огромное!!!!!! Ты лучший!!!!!',
        'photo' => '/images/feedbacks/6.jpg',
    ],
    [
        'link' => 'https://vk.com/ketrikys',
        'name' => 'Катерина Кузнецова',
        'message' => 'Привет!! Наконец-то мы добрались до инета. Выражаем свою благодарность за проделанную вами работу! Это не объяснить словами, одни эмоции! Смотрим и плачем ;), смотрим - смеемся))) Все просто супер! Ваша работа - прекрасное дополнение к воспоминаниям о дне рождении нашей семьи. СПАСИБО!',
        'photo' => '/images/feedbacks/7.jpg',
    ],
    [
        'link' => 'https://vk.com/id17113776',
        'name' => 'Екатерина Тимофеева',
        'message' => 'ЗДРАВСТВУЙТЕ АЛЕКСАНДР, НЕТ СЛОВ ЧТОБЫ ОПИСАТЬ ТЕ ЭМОЦИИ КОТОРЫЕ ВЫ НАМ ПОДАРИЛИ, ВЫ МАСТЕР СВОЕГО ДЕЛА, ВЫ ПРОСТО..., У МЕНЯ ПРАВДА НЕТ СЛОВ!!! КОГДА СМОТРЕЛИ ВИДЕО С ЛИЦА НЕ СХОДИЛА УЛЫБКА, ПОТОМ ЗАСТАВИЛИ ПЛАКАТЬ ОТ РАДОСТИ!!! СПАСИБО ВАМ ОТ РОДИТЕЛЕЙ, ТЕПЕРЬ БУДЕМ ОБРАЩАТЬСЯ ТОЛЬКО К ВАМ, ЕЩЁ РАЗ ОГРОМНОЕ ОГРОМНОЕ СПАСИБО, МЫ ВСЕ В ШОКЕ, ЧТО ВСЕ ТАКИ ЕСТЬ ЛЮДИ МАСТЕРА СВОЕГО ДЕЛА!!!',
        'photo' => '/images/feedbacks/8.jpg',
    ],
    [
        'link' => 'https://vk.com/julenochek',
        'name' => 'Юлия Самошина',
        'message' => 'Хочу еще раз сказать вам спасибо!!! Ездили в отпуск показывали всем родным и знакомым, всем очень понравилось. Говорят есть просто волшебные кадры))',
        'photo' => '/images/feedbacks/9.jpg',
    ],
    [
        'link' => 'https://vk.com/id16105967',
        'name' => 'Катерина Прозоркина',
        'message' => 'Насладились!!!!Спасибо Вам огромное, очень понравилось!!!!',
        'photo' => '/images/feedbacks/10.jpg',
    ],
];
?>

<!doctype html>
<html lang="ru-RU">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta property="og:image" content=""/>

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title>Фото Видео Студия Александра Любителева</title>
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close"></button>
                    <button class="pswp__button pswp__button--fs"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left">
                </button>
                <button class="pswp__button pswp__button--arrow--right">
                </button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>
    <link rel='dns-prefetch' href='http://maps.googleapis.com/'/>
    <link rel='dns-prefetch' href='http://s.w.org/'/>
    <link rel="stylesheet" href="css/app.css">

    <link rel='stylesheet' id='ay-style-css' href='/css/style.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='slick-css' href='/js/slick/slick.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='fotorama-css' href='/js/fotorama/fotorama.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='photoswipe-css' href='/js/photoswipe/photoswipe.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='default-skin-css'
          href='/js/photoswipe/default-skin/default-skin.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='horizontal'
          href='/css/horizontal.css' type='text/css'
          media='all'/>

    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
            integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
            crossorigin="anonymous"></script>

    <script type='text/javascript'>
        /* <![CDATA[ */
        var MyAjax = {"ajaxurl": "\/request.php"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='/js/ajax.js?ver=4.9.3'></script>

    <link rel="canonical" href="index.html"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat&subset=cyrillic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="home page-template-default page page-id-6">
<div id="page">
    <header>
        <div class="site-desc">
            <div class="tel">
                <a href="tel:+79269649878">+7 (926) 964 98 78</a>
            </div>

            <nav class="main-menu">
                <ul id="primary-menu" class="">
                    <li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13">
                        <a href="#video">Видео</a></li>
                    <li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12">
                        <a href="#photo">Фото</a></li>
                    <li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14">
                        <a href="#feedbacks">Отзывы</a></li>
                    <li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14">
                        <a href="#price">Цены</a></li>
                    <li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14">
                        <a href="#about">Обо мне</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <div id="content">
        <div>
            <div class="video-showreel">
                <div class="video_overlay">
                    <video width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
                        <source src="video/9mb.mp4">
                    </video>
                </div>
                <div class="pattern-overlay overlay-white-dots"></div>
                <div class="center-wrap">
                    <div class="center-help-wrap">
                        <div class="content-place">
                            <div style="text-align: center; font-size: 36px; line-height: 1;">
                                <p>ФОТО ВИДЕО</p>
                                <p style="font-size: .5em">студия</p>
                                <p>Александра Любителева</p>
                            </div>
                            <p>&nbsp;</p>
                            <p style="text-align: center;">
                                <a class="request" href="#request"><i id="id_one" class="button request-button"
                                                                      style="color: black">Проверить дату</i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="video-gallery-info" id="video">
            <div class="video-gallery-title">
                <h2>Видео</h2>
            </div>
            <div class="video-gallery-text">
            </div>
        </div>
        <div class="photoswipe-wrapper">
            <?php foreach ($albumVideos['body']['data'] as $albumVideo): ?>
                <div class="photoswipe-item">
                    <a href="#" data-type="video"
                       data-video='<div class="video-wrapper"><iframe class="pswp__video" src="https://player.vimeo.com<?php echo str_replace('videos', 'video', $albumVideo['uri']) ?>?title=0&byline=0&portrait=0&autoplay=0" width="960" height="640" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'>
                        <div class="video-gallery-file-thumb video-play-arrow">
                            <img class="pv" src="<?php echo $albumVideo['pictures']['sizes'][3]['link'] ?>"/>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="editor" style="background-color: white" id="photo">
            <div class="editor-title">
                <h2>Фото</h2>
            </div>
            <div class="editor-text">
                <div id='gallery-2' class='gallery galleryid-6 gallery-columns-3 gallery-size-medium'>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/1.jpg'><img
                                    width="900" height="600" src="/images/900/1.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/1.jpg 768w, /images/900/1.jpg 900w, /images/1200/1.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/2.jpg'><img
                                    width="900" height="600" src="/images/900/2.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/2.jpg 768w, /images/900/2.jpg 900w, /images/1200/2.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/3.jpg'><img
                                    width="900" height="600" src="/images/900/3.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/3.jpg 768w, /images/900/3.jpg 900w, /images/1200/3.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/4.jpg'><img
                                    width="900" height="600" src="/images/900/4.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/4.jpg 768w, /images/900/4.jpg 900w, /images/1200/4.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/5.jpg'><img
                                    width="900" height="600" src="/images/900/5.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/5.jpg 768w, /images/900/5.jpg 900w, /images/1200/5.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/6.jpg'><img
                                    width="900" height="600" src="/images/900/6.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/6.jpg 768w, /images/900/6.jpg 900w, /images/1200/6.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                    <figure class='gallery-item'><a data-size="2000x1333" href='/images/7.jpg'><img
                                    width="900" height="600" src="/images/900/7.jpg"
                                    class="attachment-medium size-medium" alt=""
                                    srcset="/images/768/7.jpg 768w, /images/900/7.jpg 900w, /images/1200/7.jpg 1200w"
                                    sizes="(max-width: 900px) 100vw, 900px"/></a></figure>
                </div>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="columns" data-sr-id="2"
             style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; transition: transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; ">
            <div class="columns-item" style="fill: #222; width: 25%;">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478.8 488.7">
                    <path d="M239.3 100C159.7 100 95 164.7 95 244.3c0 79.5 64.7 144.3 144.3 144.3s144.3-64.8 144.3-144.3C383.6 164.7 318.9 100 239.3 100zM239.3 364.1c-66 0-119.8-53.7-119.8-119.8 0-66 53.7-119.8 119.8-119.8s119.8 53.8 119.8 119.8S305.3 364.1 239.3 364.1z"></path>
                    <path d="M462.2 252.1c-4.1-5.6-4.2-13.3-0.1-19l8.5-12.1c6.7-9.5 9.1-21.5 6.4-32.9s-10.1-21.1-20.4-26.6l-13-7c-6.2-3.3-9.5-10.1-8.4-17l2.5-14.6c1.9-11.5-1.2-23.3-8.5-32.4 -7.4-9.1-18.3-14.6-29.9-15.1l-14.8-0.6c-7-0.3-13-5-14.9-11.7L365.3 49c-5-17.3-21.1-29.4-39.2-29.4 -5.5 0-11 1.1-16.1 3.3l-13.6 5.8c-6.3 2.7-14 1-18.5-4.1l-9.9-11C260.3 5 249.2 0 237.7 0c-11.8 0-23 5.1-30.8 14l-9.6 11.2c-4.5 5.2-12.1 7-18.5 4.4L165.1 24c-5-2-10.2-3.1-15.5-3.1 -18.3 0-34.5 12.3-39.3 30l-3.9 14.3c-1.8 6.7-7.8 11.5-14.8 11.9L76.9 78c-11.6 0.6-22.5 6.3-29.7 15.5s-10.1 21.1-8 32.5l2.7 14.5c1.3 6.9-2 13.8-8.1 17.2l-12.9 7.2c-10.2 5.7-17.5 15.5-20 26.9s0 23.4 6.9 32.8l8.7 11.9c4.1 5.6 4.2 13.3 0.1 19l-8.5 12.1c-6.7 9.5-9.1 21.5-6.4 32.9s10.1 21.1 20.4 26.6l13 7c6.2 3.3 9.5 10.1 8.4 17L41 365.7c-1.9 11.5 1.2 23.3 8.5 32.4 7.4 9.1 18.3 14.6 29.9 15.1l14.8 0.6c7 0.3 13 5 14.9 11.7l4.1 14.2c5 17.3 21.1 29.4 39.2 29.4 5.5 0 11-1.1 16.1-3.3l13.6-5.8c6.3-2.7 14-1 18.5 4.1l9.9 11c7.7 8.6 18.8 13.6 30.3 13.6 11.8 0 23-5.1 30.8-14l9.7-11.2c4.5-5.2 12.1-7 18.5-4.4l13.7 5.6c5 2 10.2 3.1 15.5 3.1 18.3 0 34.5-12.3 39.3-30l3.9-14.3c1.8-6.7 7.8-11.5 14.8-11.9l14.8-0.9c11.7-0.7 22.5-6.3 29.7-15.5s10.1-21 8-32.5l-2.7-14.5c-1.3-6.9 2-13.8 8.1-17.2l12.9-7.2c10.2-5.7 17.5-15.5 20-26.9s0-23.4-6.9-32.8L462.2 252.1zM453.9 291.6c-1 4.6-3.9 8.4-8 10.7l-12.9 7.2c-15.3 8.5-23.5 25.8-20.3 43l2.7 14.5c0.9 4.7-0.3 9.3-3.2 13 -2.9 3.7-7.1 5.9-11.9 6.2l-14.7 0.9c-17.5 1-32.4 13-37 29.9l-3.9 14.2c-1.9 7.1-8.4 12-15.7 12 -2.1 0-4.2-0.4-6.2-1.2l-13.7-5.6c-5-2-10.2-3.1-15.5-3.1 -11.8 0-23 5.1-30.8 14l-9.7 11.1c-3.1 3.6-7.5 5.6-12.3 5.6 -4.7 0-9-1.9-12.1-5.4l-9.9-11C211.1 439 200 434 188.4 434c-5.6 0-11 1.1-16.1 3.3l-13.6 5.8c-2.1 0.9-4.2 1.3-6.4 1.3 -7.2 0-13.6-4.8-15.6-11.7l-4.1-14.2c-4.9-16.8-19.9-28.6-37.4-29.4l-14.8-0.6c-4.7-0.2-9-2.3-11.9-6 -3-3.7-4.2-8.3-3.4-12.9l2.5-14.6c2.9-17.3-5.5-34.4-21-42.7l-13-7c-4.2-2.2-7.1-6-8.2-10.6s-0.2-9.3 2.6-13.1l8.5-12.1c10.1-14.3 10-33.4-0.4-47.6L27.4 210c-2.8-3.8-3.8-8.5-2.7-13.1 1-4.6 3.9-8.4 8-10.7l12.9-7.2c15.3-8.5 23.5-25.8 20.3-43l-2.6-14.4c-0.9-4.7 0.3-9.3 3.2-13 2.9-3.7 7.1-5.9 11.9-6.2l14.7-0.9c17.5-1 32.3-13 37-29.9l3.9-14.3c1.9-7.1 8.4-12 15.7-12 2.1 0 4.2 0.4 6.2 1.2l13.7 5.6c5 2 10.2 3.1 15.5 3.1 11.8 0 23-5.1 30.8-14l9.7-11.1c3.1-3.6 7.5-5.6 12.3-5.6 4.7 0 9 1.9 12.1 5.4l9.9 11c7.7 8.6 18.8 13.6 30.4 13.6 5.6 0 11-1.1 16.1-3.3l13.6-5.8c2.1-0.9 4.2-1.3 6.4-1.3 7.2 0 13.6 4.8 15.6 11.7l4.1 14.2c4.9 16.8 19.9 28.6 37.4 29.4l14.8 0.6c4.7 0.2 9 2.3 12 6s4.2 8.3 3.4 12.9l-2.5 14.6c-2.9 17.3 5.5 34.4 21 42.7l13 7c4.2 2.2 7.1 6 8.2 10.6s0.2 9.3-2.5 13.1l-8.5 12.1c-10.1 14.3-10 33.4 0.4 47.6l8.7 11.9C453.9 282.3 454.9 287 453.9 291.6z"></path>
                    <path d="M274.8 204.6l-53.6 53.3 -17.4-17.5c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5-0.1 17.3l26.1 26.2c2.3 2.3 5.4 3.6 8.7 3.6l0 0c3.2 0 6.3-1.3 8.6-3.6l62.3-61.9c4.8-4.8 4.8-12.5 0.1-17.3C287.4 199.9 279.6 199.9 274.8 204.6z"></path>
                </svg>
                <div class="columns-item-content" style="background-color: #fff">
                    <p class="promo"><strong>9 лет</strong></p>
                    <p style="text-align: center;">в свадебной сфере</p>
                </div>
            </div>

            <div class="columns-item" style="fill: #222; width: 25%;">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478.8 488.7">
                    <path d="M239.3 100C159.7 100 95 164.7 95 244.3c0 79.5 64.7 144.3 144.3 144.3s144.3-64.8 144.3-144.3C383.6 164.7 318.9 100 239.3 100zM239.3 364.1c-66 0-119.8-53.7-119.8-119.8 0-66 53.7-119.8 119.8-119.8s119.8 53.8 119.8 119.8S305.3 364.1 239.3 364.1z"></path>
                    <path d="M462.2 252.1c-4.1-5.6-4.2-13.3-0.1-19l8.5-12.1c6.7-9.5 9.1-21.5 6.4-32.9s-10.1-21.1-20.4-26.6l-13-7c-6.2-3.3-9.5-10.1-8.4-17l2.5-14.6c1.9-11.5-1.2-23.3-8.5-32.4 -7.4-9.1-18.3-14.6-29.9-15.1l-14.8-0.6c-7-0.3-13-5-14.9-11.7L365.3 49c-5-17.3-21.1-29.4-39.2-29.4 -5.5 0-11 1.1-16.1 3.3l-13.6 5.8c-6.3 2.7-14 1-18.5-4.1l-9.9-11C260.3 5 249.2 0 237.7 0c-11.8 0-23 5.1-30.8 14l-9.6 11.2c-4.5 5.2-12.1 7-18.5 4.4L165.1 24c-5-2-10.2-3.1-15.5-3.1 -18.3 0-34.5 12.3-39.3 30l-3.9 14.3c-1.8 6.7-7.8 11.5-14.8 11.9L76.9 78c-11.6 0.6-22.5 6.3-29.7 15.5s-10.1 21.1-8 32.5l2.7 14.5c1.3 6.9-2 13.8-8.1 17.2l-12.9 7.2c-10.2 5.7-17.5 15.5-20 26.9s0 23.4 6.9 32.8l8.7 11.9c4.1 5.6 4.2 13.3 0.1 19l-8.5 12.1c-6.7 9.5-9.1 21.5-6.4 32.9s10.1 21.1 20.4 26.6l13 7c6.2 3.3 9.5 10.1 8.4 17L41 365.7c-1.9 11.5 1.2 23.3 8.5 32.4 7.4 9.1 18.3 14.6 29.9 15.1l14.8 0.6c7 0.3 13 5 14.9 11.7l4.1 14.2c5 17.3 21.1 29.4 39.2 29.4 5.5 0 11-1.1 16.1-3.3l13.6-5.8c6.3-2.7 14-1 18.5 4.1l9.9 11c7.7 8.6 18.8 13.6 30.3 13.6 11.8 0 23-5.1 30.8-14l9.7-11.2c4.5-5.2 12.1-7 18.5-4.4l13.7 5.6c5 2 10.2 3.1 15.5 3.1 18.3 0 34.5-12.3 39.3-30l3.9-14.3c1.8-6.7 7.8-11.5 14.8-11.9l14.8-0.9c11.7-0.7 22.5-6.3 29.7-15.5s10.1-21 8-32.5l-2.7-14.5c-1.3-6.9 2-13.8 8.1-17.2l12.9-7.2c10.2-5.7 17.5-15.5 20-26.9s0-23.4-6.9-32.8L462.2 252.1zM453.9 291.6c-1 4.6-3.9 8.4-8 10.7l-12.9 7.2c-15.3 8.5-23.5 25.8-20.3 43l2.7 14.5c0.9 4.7-0.3 9.3-3.2 13 -2.9 3.7-7.1 5.9-11.9 6.2l-14.7 0.9c-17.5 1-32.4 13-37 29.9l-3.9 14.2c-1.9 7.1-8.4 12-15.7 12 -2.1 0-4.2-0.4-6.2-1.2l-13.7-5.6c-5-2-10.2-3.1-15.5-3.1 -11.8 0-23 5.1-30.8 14l-9.7 11.1c-3.1 3.6-7.5 5.6-12.3 5.6 -4.7 0-9-1.9-12.1-5.4l-9.9-11C211.1 439 200 434 188.4 434c-5.6 0-11 1.1-16.1 3.3l-13.6 5.8c-2.1 0.9-4.2 1.3-6.4 1.3 -7.2 0-13.6-4.8-15.6-11.7l-4.1-14.2c-4.9-16.8-19.9-28.6-37.4-29.4l-14.8-0.6c-4.7-0.2-9-2.3-11.9-6 -3-3.7-4.2-8.3-3.4-12.9l2.5-14.6c2.9-17.3-5.5-34.4-21-42.7l-13-7c-4.2-2.2-7.1-6-8.2-10.6s-0.2-9.3 2.6-13.1l8.5-12.1c10.1-14.3 10-33.4-0.4-47.6L27.4 210c-2.8-3.8-3.8-8.5-2.7-13.1 1-4.6 3.9-8.4 8-10.7l12.9-7.2c15.3-8.5 23.5-25.8 20.3-43l-2.6-14.4c-0.9-4.7 0.3-9.3 3.2-13 2.9-3.7 7.1-5.9 11.9-6.2l14.7-0.9c17.5-1 32.3-13 37-29.9l3.9-14.3c1.9-7.1 8.4-12 15.7-12 2.1 0 4.2 0.4 6.2 1.2l13.7 5.6c5 2 10.2 3.1 15.5 3.1 11.8 0 23-5.1 30.8-14l9.7-11.1c3.1-3.6 7.5-5.6 12.3-5.6 4.7 0 9 1.9 12.1 5.4l9.9 11c7.7 8.6 18.8 13.6 30.4 13.6 5.6 0 11-1.1 16.1-3.3l13.6-5.8c2.1-0.9 4.2-1.3 6.4-1.3 7.2 0 13.6 4.8 15.6 11.7l4.1 14.2c4.9 16.8 19.9 28.6 37.4 29.4l14.8 0.6c4.7 0.2 9 2.3 12 6s4.2 8.3 3.4 12.9l-2.5 14.6c-2.9 17.3 5.5 34.4 21 42.7l13 7c4.2 2.2 7.1 6 8.2 10.6s0.2 9.3-2.5 13.1l-8.5 12.1c-10.1 14.3-10 33.4 0.4 47.6l8.7 11.9C453.9 282.3 454.9 287 453.9 291.6z"></path>
                    <path d="M274.8 204.6l-53.6 53.3 -17.4-17.5c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5-0.1 17.3l26.1 26.2c2.3 2.3 5.4 3.6 8.7 3.6l0 0c3.2 0 6.3-1.3 8.6-3.6l62.3-61.9c4.8-4.8 4.8-12.5 0.1-17.3C287.4 199.9 279.6 199.9 274.8 204.6z"></path>
                </svg>
                <div class="columns-item-content" style="background-color: #fff">
                    <p class="promo"><strong>13 стран</strong></p>
                    <p style="text-align: center;">Европы и Мира</p>
                </div>

            </div>

            <div class="columns-item" style="fill: #222; width: 25%;">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478.8 488.7">
                    <path d="M239.3 100C159.7 100 95 164.7 95 244.3c0 79.5 64.7 144.3 144.3 144.3s144.3-64.8 144.3-144.3C383.6 164.7 318.9 100 239.3 100zM239.3 364.1c-66 0-119.8-53.7-119.8-119.8 0-66 53.7-119.8 119.8-119.8s119.8 53.8 119.8 119.8S305.3 364.1 239.3 364.1z"></path>
                    <path d="M462.2 252.1c-4.1-5.6-4.2-13.3-0.1-19l8.5-12.1c6.7-9.5 9.1-21.5 6.4-32.9s-10.1-21.1-20.4-26.6l-13-7c-6.2-3.3-9.5-10.1-8.4-17l2.5-14.6c1.9-11.5-1.2-23.3-8.5-32.4 -7.4-9.1-18.3-14.6-29.9-15.1l-14.8-0.6c-7-0.3-13-5-14.9-11.7L365.3 49c-5-17.3-21.1-29.4-39.2-29.4 -5.5 0-11 1.1-16.1 3.3l-13.6 5.8c-6.3 2.7-14 1-18.5-4.1l-9.9-11C260.3 5 249.2 0 237.7 0c-11.8 0-23 5.1-30.8 14l-9.6 11.2c-4.5 5.2-12.1 7-18.5 4.4L165.1 24c-5-2-10.2-3.1-15.5-3.1 -18.3 0-34.5 12.3-39.3 30l-3.9 14.3c-1.8 6.7-7.8 11.5-14.8 11.9L76.9 78c-11.6 0.6-22.5 6.3-29.7 15.5s-10.1 21.1-8 32.5l2.7 14.5c1.3 6.9-2 13.8-8.1 17.2l-12.9 7.2c-10.2 5.7-17.5 15.5-20 26.9s0 23.4 6.9 32.8l8.7 11.9c4.1 5.6 4.2 13.3 0.1 19l-8.5 12.1c-6.7 9.5-9.1 21.5-6.4 32.9s10.1 21.1 20.4 26.6l13 7c6.2 3.3 9.5 10.1 8.4 17L41 365.7c-1.9 11.5 1.2 23.3 8.5 32.4 7.4 9.1 18.3 14.6 29.9 15.1l14.8 0.6c7 0.3 13 5 14.9 11.7l4.1 14.2c5 17.3 21.1 29.4 39.2 29.4 5.5 0 11-1.1 16.1-3.3l13.6-5.8c6.3-2.7 14-1 18.5 4.1l9.9 11c7.7 8.6 18.8 13.6 30.3 13.6 11.8 0 23-5.1 30.8-14l9.7-11.2c4.5-5.2 12.1-7 18.5-4.4l13.7 5.6c5 2 10.2 3.1 15.5 3.1 18.3 0 34.5-12.3 39.3-30l3.9-14.3c1.8-6.7 7.8-11.5 14.8-11.9l14.8-0.9c11.7-0.7 22.5-6.3 29.7-15.5s10.1-21 8-32.5l-2.7-14.5c-1.3-6.9 2-13.8 8.1-17.2l12.9-7.2c10.2-5.7 17.5-15.5 20-26.9s0-23.4-6.9-32.8L462.2 252.1zM453.9 291.6c-1 4.6-3.9 8.4-8 10.7l-12.9 7.2c-15.3 8.5-23.5 25.8-20.3 43l2.7 14.5c0.9 4.7-0.3 9.3-3.2 13 -2.9 3.7-7.1 5.9-11.9 6.2l-14.7 0.9c-17.5 1-32.4 13-37 29.9l-3.9 14.2c-1.9 7.1-8.4 12-15.7 12 -2.1 0-4.2-0.4-6.2-1.2l-13.7-5.6c-5-2-10.2-3.1-15.5-3.1 -11.8 0-23 5.1-30.8 14l-9.7 11.1c-3.1 3.6-7.5 5.6-12.3 5.6 -4.7 0-9-1.9-12.1-5.4l-9.9-11C211.1 439 200 434 188.4 434c-5.6 0-11 1.1-16.1 3.3l-13.6 5.8c-2.1 0.9-4.2 1.3-6.4 1.3 -7.2 0-13.6-4.8-15.6-11.7l-4.1-14.2c-4.9-16.8-19.9-28.6-37.4-29.4l-14.8-0.6c-4.7-0.2-9-2.3-11.9-6 -3-3.7-4.2-8.3-3.4-12.9l2.5-14.6c2.9-17.3-5.5-34.4-21-42.7l-13-7c-4.2-2.2-7.1-6-8.2-10.6s-0.2-9.3 2.6-13.1l8.5-12.1c10.1-14.3 10-33.4-0.4-47.6L27.4 210c-2.8-3.8-3.8-8.5-2.7-13.1 1-4.6 3.9-8.4 8-10.7l12.9-7.2c15.3-8.5 23.5-25.8 20.3-43l-2.6-14.4c-0.9-4.7 0.3-9.3 3.2-13 2.9-3.7 7.1-5.9 11.9-6.2l14.7-0.9c17.5-1 32.3-13 37-29.9l3.9-14.3c1.9-7.1 8.4-12 15.7-12 2.1 0 4.2 0.4 6.2 1.2l13.7 5.6c5 2 10.2 3.1 15.5 3.1 11.8 0 23-5.1 30.8-14l9.7-11.1c3.1-3.6 7.5-5.6 12.3-5.6 4.7 0 9 1.9 12.1 5.4l9.9 11c7.7 8.6 18.8 13.6 30.4 13.6 5.6 0 11-1.1 16.1-3.3l13.6-5.8c2.1-0.9 4.2-1.3 6.4-1.3 7.2 0 13.6 4.8 15.6 11.7l4.1 14.2c4.9 16.8 19.9 28.6 37.4 29.4l14.8 0.6c4.7 0.2 9 2.3 12 6s4.2 8.3 3.4 12.9l-2.5 14.6c-2.9 17.3 5.5 34.4 21 42.7l13 7c4.2 2.2 7.1 6 8.2 10.6s0.2 9.3-2.5 13.1l-8.5 12.1c-10.1 14.3-10 33.4 0.4 47.6l8.7 11.9C453.9 282.3 454.9 287 453.9 291.6z"></path>
                    <path d="M274.8 204.6l-53.6 53.3 -17.4-17.5c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5-0.1 17.3l26.1 26.2c2.3 2.3 5.4 3.6 8.7 3.6l0 0c3.2 0 6.3-1.3 8.6-3.6l62.3-61.9c4.8-4.8 4.8-12.5 0.1-17.3C287.4 199.9 279.6 199.9 274.8 204.6z"></path>
                </svg>
                <div class="columns-item-content" style="background-color: #fff">
                    <p class="promo"><strong>78 городов</strong></p>
                    <p style="text-align: center;">попали в кадр</p>
                </div>
            </div>

            <div class="columns-item" style="fill: #222; width: 25%;">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478.8 488.7">
                    <path d="M239.3 100C159.7 100 95 164.7 95 244.3c0 79.5 64.7 144.3 144.3 144.3s144.3-64.8 144.3-144.3C383.6 164.7 318.9 100 239.3 100zM239.3 364.1c-66 0-119.8-53.7-119.8-119.8 0-66 53.7-119.8 119.8-119.8s119.8 53.8 119.8 119.8S305.3 364.1 239.3 364.1z"></path>
                    <path d="M462.2 252.1c-4.1-5.6-4.2-13.3-0.1-19l8.5-12.1c6.7-9.5 9.1-21.5 6.4-32.9s-10.1-21.1-20.4-26.6l-13-7c-6.2-3.3-9.5-10.1-8.4-17l2.5-14.6c1.9-11.5-1.2-23.3-8.5-32.4 -7.4-9.1-18.3-14.6-29.9-15.1l-14.8-0.6c-7-0.3-13-5-14.9-11.7L365.3 49c-5-17.3-21.1-29.4-39.2-29.4 -5.5 0-11 1.1-16.1 3.3l-13.6 5.8c-6.3 2.7-14 1-18.5-4.1l-9.9-11C260.3 5 249.2 0 237.7 0c-11.8 0-23 5.1-30.8 14l-9.6 11.2c-4.5 5.2-12.1 7-18.5 4.4L165.1 24c-5-2-10.2-3.1-15.5-3.1 -18.3 0-34.5 12.3-39.3 30l-3.9 14.3c-1.8 6.7-7.8 11.5-14.8 11.9L76.9 78c-11.6 0.6-22.5 6.3-29.7 15.5s-10.1 21.1-8 32.5l2.7 14.5c1.3 6.9-2 13.8-8.1 17.2l-12.9 7.2c-10.2 5.7-17.5 15.5-20 26.9s0 23.4 6.9 32.8l8.7 11.9c4.1 5.6 4.2 13.3 0.1 19l-8.5 12.1c-6.7 9.5-9.1 21.5-6.4 32.9s10.1 21.1 20.4 26.6l13 7c6.2 3.3 9.5 10.1 8.4 17L41 365.7c-1.9 11.5 1.2 23.3 8.5 32.4 7.4 9.1 18.3 14.6 29.9 15.1l14.8 0.6c7 0.3 13 5 14.9 11.7l4.1 14.2c5 17.3 21.1 29.4 39.2 29.4 5.5 0 11-1.1 16.1-3.3l13.6-5.8c6.3-2.7 14-1 18.5 4.1l9.9 11c7.7 8.6 18.8 13.6 30.3 13.6 11.8 0 23-5.1 30.8-14l9.7-11.2c4.5-5.2 12.1-7 18.5-4.4l13.7 5.6c5 2 10.2 3.1 15.5 3.1 18.3 0 34.5-12.3 39.3-30l3.9-14.3c1.8-6.7 7.8-11.5 14.8-11.9l14.8-0.9c11.7-0.7 22.5-6.3 29.7-15.5s10.1-21 8-32.5l-2.7-14.5c-1.3-6.9 2-13.8 8.1-17.2l12.9-7.2c10.2-5.7 17.5-15.5 20-26.9s0-23.4-6.9-32.8L462.2 252.1zM453.9 291.6c-1 4.6-3.9 8.4-8 10.7l-12.9 7.2c-15.3 8.5-23.5 25.8-20.3 43l2.7 14.5c0.9 4.7-0.3 9.3-3.2 13 -2.9 3.7-7.1 5.9-11.9 6.2l-14.7 0.9c-17.5 1-32.4 13-37 29.9l-3.9 14.2c-1.9 7.1-8.4 12-15.7 12 -2.1 0-4.2-0.4-6.2-1.2l-13.7-5.6c-5-2-10.2-3.1-15.5-3.1 -11.8 0-23 5.1-30.8 14l-9.7 11.1c-3.1 3.6-7.5 5.6-12.3 5.6 -4.7 0-9-1.9-12.1-5.4l-9.9-11C211.1 439 200 434 188.4 434c-5.6 0-11 1.1-16.1 3.3l-13.6 5.8c-2.1 0.9-4.2 1.3-6.4 1.3 -7.2 0-13.6-4.8-15.6-11.7l-4.1-14.2c-4.9-16.8-19.9-28.6-37.4-29.4l-14.8-0.6c-4.7-0.2-9-2.3-11.9-6 -3-3.7-4.2-8.3-3.4-12.9l2.5-14.6c2.9-17.3-5.5-34.4-21-42.7l-13-7c-4.2-2.2-7.1-6-8.2-10.6s-0.2-9.3 2.6-13.1l8.5-12.1c10.1-14.3 10-33.4-0.4-47.6L27.4 210c-2.8-3.8-3.8-8.5-2.7-13.1 1-4.6 3.9-8.4 8-10.7l12.9-7.2c15.3-8.5 23.5-25.8 20.3-43l-2.6-14.4c-0.9-4.7 0.3-9.3 3.2-13 2.9-3.7 7.1-5.9 11.9-6.2l14.7-0.9c17.5-1 32.3-13 37-29.9l3.9-14.3c1.9-7.1 8.4-12 15.7-12 2.1 0 4.2 0.4 6.2 1.2l13.7 5.6c5 2 10.2 3.1 15.5 3.1 11.8 0 23-5.1 30.8-14l9.7-11.1c3.1-3.6 7.5-5.6 12.3-5.6 4.7 0 9 1.9 12.1 5.4l9.9 11c7.7 8.6 18.8 13.6 30.4 13.6 5.6 0 11-1.1 16.1-3.3l13.6-5.8c2.1-0.9 4.2-1.3 6.4-1.3 7.2 0 13.6 4.8 15.6 11.7l4.1 14.2c4.9 16.8 19.9 28.6 37.4 29.4l14.8 0.6c4.7 0.2 9 2.3 12 6s4.2 8.3 3.4 12.9l-2.5 14.6c-2.9 17.3 5.5 34.4 21 42.7l13 7c4.2 2.2 7.1 6 8.2 10.6s0.2 9.3-2.5 13.1l-8.5 12.1c-10.1 14.3-10 33.4 0.4 47.6l8.7 11.9C453.9 282.3 454.9 287 453.9 291.6z"></path>
                    <path d="M274.8 204.6l-53.6 53.3 -17.4-17.5c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5-0.1 17.3l26.1 26.2c2.3 2.3 5.4 3.6 8.7 3.6l0 0c3.2 0 6.3-1.3 8.6-3.6l62.3-61.9c4.8-4.8 4.8-12.5 0.1-17.3C287.4 199.9 279.6 199.9 274.8 204.6z"></path>
                </svg>
                <div class="columns-item-content" style="background-color: #fff">
                    <p class="promo"><strong>350</strong></p>
                    <p style="text-align: center;">счастливых семей</p>
                </div>
            </div>
        </div>

        <div class="columns-info" id="feedbacks">
            <div class="columns-title">
                <h2>Отзывы</h2>
            </div>
        </div>

        <div class="feedbacks">
            <div class="scrollbar">
                <div class="handle">
                    <div class="mousearea"></div>
                </div>
            </div>

            <button class="prev"><i class="fa fa-angle-left"></i></button>
            <button class="next"><i class="fa fa-angle-right"></i></button>

            <div class="frame" id="centered">
                <ul class="clearfix">
                    <?php foreach ($feedbacks as $feedback) : ?>
                        <li class="feedback">
                            <p class="feedback-title">
                                <a target="_blank"
                                   href="<?php echo $feedback['link']; ?>"><?php echo $feedback['name']; ?></a>
                            </p>
                            <hr>
                            <p class="feedback-body activity_rounded">
                                <a target="_blank" href="<?php echo $feedback['link']; ?>"><img
                                            class="avatar imgleft clip-circle" src="<?php echo $feedback['photo'] ?>"
                                            alt=""></a>
                                <span class="more"><?php echo $feedback['message']; ?></span>
                            </p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <ul class="pages"></ul>
        </div>

        <div class="columns-info" id="feedbacks">
            <div class="columns-title">
                <h2>Клиенты</h2>
            </div>
        </div>

        <div class="companies clearfix">
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/01.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/02.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/03.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/04.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/05.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/06.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/07.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/08.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/09.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="square">
                <div class="content">
                    <div class="table">
                        <div class="table-cell">
                            <img class="rs" src="/images/companies/10.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="columns-info" id="price">
            <div class="columns-title">
                <h2>Цены</h2>
            </div>
        </div>
        <div class="columns" style="flex-wrap:wrap-reverse">
            <div class="columns-item" style="fill: #222; width: 33.33%;">
                <div class="columns-item-content" style="background-color: #ededed">
                    <p style="text-align: center;">
                        <span class="price header">ПРОСТОЙ</span>
                    </p>
                    <p>&nbsp;</p>
                    <p style="text-align: center;">
                        <span class="price amount">15 000 &#8381;</span></p>
                    <p>&nbsp;</p>
                    <ul>
                        <li style="text-align: left;"><span class="price">1 оператор</span></li>
                        <li style="text-align: left;"><span class="price">3 часа съемки</span></li>
                        <li style="text-align: left;"><span class="price">Клип 3-5 минут</span></li>
                        <li style="text-align: left;"><span class="price">Фильм 15-20 минут</span></li>
                        <li style="text-align: left;"><span class="price">Инстаграм-ролик - 1 шт.​</span></li>
                        <li style="text-align: left;"><span class="price">Готовность - 7-10 дней</span></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p style="text-align: center;">
                        <span class="price">
                             <a class="request" href="#request"><i id="id_one" class="button request-button"
                                                                   style="color: black">Проверить дату</i></a>
                        </span>
                    </p>
                </div>
            </div>
            <div class="columns-item" style="fill: #222; width: 33.33%;">
                <div class="columns-item-content" style="background-color: #ededed">
                    <p style="text-align: center;"><span class="price header">ПОПУЛЯРНЫЙ</span></p>
                    <p>&nbsp;</p>
                    <p style="text-align: center;"><span class="price amount">35 000 &#8381;</span></p>
                    <p>&nbsp;</p>
                    <ul>
                        <li><span class="price">1 оператор</span></li>
                        <li><span class="price">8 часов съемки</span></li>
                        <li><span class="price">Клип - 3-5 минут</span></li>
                        <li><span class="price">Фильм - 30-40 минут</span></li>
                        <li><span class="price">Инстаграм-ролик - 3 шт.</span></li>
                        <li><span class="price">Готовность - 7-10 дней</span></li>
                        <li><span class="price">Видеоприглашение</span></li>
                        <li><span class="price">Скидка 50% на аэросъемку.</span></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p style="text-align: center;">
                        <span class="price">
                             <a class="request" href="#request"><i id="id_one" class="button request-button"
                                                                   style="color: black">Проверить дату</i></a>
                        </span>
                    </p>
                </div>
            </div>
            <div class="columns-item" style="fill: #222; width: 33.33%;">

                <div class="columns-item-content" style="background-color: #ededed">

                    <p style="text-align: center;"><span class="price header">ПРЕМИУМ</span></p>
                    <p>&nbsp;</p>
                    <p style="text-align: center;"><span class="price amount">60 000 &#8381;</span></p>
                    <p>&nbsp;</p>
                    <ul>
                        <li><span class="price">2 оператора</span></li>
                        <li><span class="price">10 часов съемки</span></li>
                        <li><span class="price">Клип - 3-5 минут</span></li>
                        <li><span class="price">Фильм - 40-60 минут</span></li>
                        <li><span class="price">Инстаграм-ролик - 5 шт.</span></li>
                        <li><span class="price">Готовность - 7-10 дней</span></li>
                        <li><span class="price">Видеоприглашение</span></li>
                        <li><span class="price">Аэросъемка</span></li>
                        <li><span class="price">Love Story</span></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p style="text-align: center;">
                        <span class="price">
                             <a class="request" href="#request"><i id="id_one" class="button request-button"
                                                                   style="color: black">Проверить дату</i></a>
                        </span>
                    </p>
                </div>
            </div>
        </div>

        <div class="columns-info">
            <div class="columns-title">
                <h3>Не нашли, что искали? - Звоните, обсудим.</h3>
            </div>
        </div>


        <div class="columns-info" id="about">
            <div class="columns-title">
                <h2>Обо мне</h2>
            </div>
        </div>
        <div class="columns">
            <div class="columns-item" style="fill: #222; width: 50%;">
                <div class="columns-item-content" style="background-color: #fff">
                    <p>
                        Этот сайт посвящен моей работе, которая, так уж случилось, является моим самым любимым
                        делом,
                        хобби, призванием и прочие синонимы.
                    </p>
                    <p>
                        За моими плечами 9 лет работы и более 300 свадеб, в разных частях России и Мира. Но самое
                        главное, что
                        каждая свадьба была прекрасна и наполнена смехом, улыбками и яркими эмоциями, которые
                        остаются
                        на видео.
                    </p>
                    <p>
                        Ваш персональный видеофильм - это рассказ, наполненный самыми важными воспоминаниями,
                        чувствами
                        и непередаваемой энергетикой влюбленных людей.
                    </p>
                    <p>
                        ​Все детали мы обговорим заранее и выберем самое правильное направление! Пишите, звоните,
                        задавайте любые вопросы.
                    </p>
                </div>
            </div>

            <div class="columns-item" style="fill: #222; width: 50%;">
                <div class="columns-item-content" style="background-color: #fff">
                    <img src="/images/author.jpg" alt="Александр Любителев" class="imgright">
                </div>
            </div>
        </div>

        <!-- CONTACTS -->
        <div class="columns-info" id="contacts">
            <div class="columns-title">
                <h2>Контакты</h2>
            </div>
        </div>
        <div class="columns">
            <div class="columns-item" style="fill: #222; width: 100%;">
                <div class="columns-item-content" style="background-color: #fff">
                    Метро Тверская, Большой Гнездниковский переулок 7, оф. 307
                </div>
            </div>
        </div>
        <div class="google-map">
            <div class="marker" data-lat="55.763569" data-lng="37.605182"></div>
        </div>

        <div class="columns-info" id="request">
            <div class="columns-title">
                <h2>Проверить дату</h2>
            </div>
        </div>
        <div class="columns">
            <div class="columns-item" style="fill: #222; width: 100%;">
                <div class="columns-item-content">
                    <div role="form" class="wpcf7" lang="ru-RU" dir="ltr">
                        <form id="form" method="post" action="/request.php" data-parsley-validate>
                            <p><label> Ваше имя<br/>
                                    <span class="">
                                <input type="text"
                                       name="name"
                                       id="name"
                                       value=""
                                       size="40"
                                       data-parsley-required="true"
                                />
                            </span>
                                </label>
                            </p>
                            <p><label> Ваш e-mail<br/>
                                    <span class="">
                                <input type="email"
                                       name="email"
                                       value=""
                                       size="40"
                                       data-parsley-required="true"
                                       data-parsley-type="email"
                                />
                            </span>
                                </label>
                            </p>
                            <p><label> Ваш телефон<br/>
                                    <span class="">
                                <input type="tel"
                                       name="phone"
                                       value=""
                                       size="40"
                                       data-parsley-required="true"
                                       data-parsley-minlength="10"
                                />
                            </span> </label>
                            </p>
                            <p><label> Дата мероприятия<br/>
                                    <span class="">
                                <input type="text"
                                       name="event-date"
                                       data-parsley-required="true"
                                >
                            </span>
                                </label>
                            </p>
                            <p><label> Комментарий<br/>
                                    <span class="">
                                <textarea name="message"></textarea>
                            </span>
                                </label>
                            </p>
                            <p><input type="submit" value="Отправить" class=""/></p>
                        </form>
                    </div>
                    <br>
                    <h3 id="myResultsDiv"></h3>
                </div>
            </div>
        </div>
        <a class="scrollup"></a>
    </div><!-- #content -->

    <footer class="footer">
        <a class="logo" href='index.html' title='Александр Любителев' rel='home'>
            <span>Александр Любителев</span>
        </a>

        <div class="footer-text">
            <i>Свадебная фото и видеосъемка, <a href="tel:+79269649878">+7 (926) 964 98 78</a></i>

            <p>
                <a class="social-icon" href="https://vk.com/lyubitelev" target="_blank"><i style="font-size:24px"
                                                                                           class="fa fa-vk"></i> </a>
                <a class="social-icon" href="https://www.facebook.com/videomoscow" target="_blank"><i
                            style="font-size:24px" class="fa fa-facebook"></i></a>
                <a class="social-icon" href="https://www.instagram.com/wedding_video_moscow/" target="_blank"><i
                            style="font-size:24px" class="fa fa-instagram"></i></a>
                <a class="social-icon" href="https://vimeo.com/videofilm" target="_blank"><i style="font-size:24px"
                                                                                             class="fa fa-vimeo"></i></a>
                <a class="social-icon" href="whatsapp://send?phone=79269649878" target="_top"><i style="font-size:24px"
                                                                                                 class="fa fa-whatsapp"></i></a>
            </p>

        </div>

        <script type='text/javascript'>
            /* <![CDATA[ */
            var wpcf7 = {
                "apiSettings": {"root": "\/request.php", "namespace": ""},
                "recaptcha": {"messages": {"empty": "\u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0434\u0438\u0442\u0435, \u0447\u0442\u043e \u0432\u044b \u043d\u0435 \u0440\u043e\u0431\u043e\u0442."}}
            };
            /* ]]> */
        </script>

        <script type='text/javascript' src='/js/slick/slick.min.js'></script>
        <script type='text/javascript' src='/js/fotorama/fotorama.js'></script>
        <script type='text/javascript' src='/js/js.js'></script>
        <script type='text/javascript'
                src='/js/scrollreveal/scrollreveal.min.js'></script>
        <script type='text/javascript'
                src='/js/scrollreveal/scrollreveal.init.js'></script>
        <script type='text/javascript' src='/js/scrl.js'></script>
        <script type='text/javascript'
                src='https://maps.googleapis.com/maps/api/js?key=AIzaSyD-8tDULP14Uf1oB3-LfkaO_Ed6DPfSJm8'></script>
        <script type='text/javascript' src='/js/google-map.js'></script>
        <script type='text/javascript'
                src='/js/justified/justifiedGallery.min.js'></script>
        <script type='text/javascript'
                src='/js/justified/justifiedGallery.init.js'></script>
        <script type='text/javascript' src='/js/photoswipe/photoswipe.js'></script>
        <script type='text/javascript'
                src='/js/photoswipe/photoswipe-ui-default.js'></script>
        <script type='text/javascript' src='/js/photoswipe/photoswipe.init.js'></script>
        <script type='text/javascript' src='/js/photoswipe/photoswipe.video.js'></script>
        <script type="text/javascript" src="/js/sly.js"></script>
        <script type="text/javascript" src="/js/parsley.min.js"></script>
        <script type="text/javascript" src="/js/i18n/ru.js"></script>
        <script type="text/javascript" src="/js/horizontal.js"></script>
        <script type="text/javascript" src="/js/app.js"></script>

    </footer>
</div><!-- #page -->

</body>
</html>